package com.example.crudwahyu.API;

import com.example.crudwahyu.Model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIREquestData {
    @GET("/api/users")
    Call<ResponseModel> getUser();
}
